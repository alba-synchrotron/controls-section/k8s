# K8s PoC for Tango CS at ALBA

This is a "toy" project to create a Proof of Concept of a k8s-based Tango control system for a BL.

**It is Work-in-Progress**

We are using SKA's project as basis:

https://github.com/ska-telescope/ska-docker


Goals:
- deploy a miocroservices-based tango control system using k8s / Helm
- allow an out-of-cluster client machine to interact with the deployed tango system
- serve GUIs to an out-of-cluster client machine 

## How to set it up

We are using locally installed [minikube](https://minikube.sigs.k8s.io/docs/start/)

```
git clone <this repo>
cd k8s
minikube start
minikube addons enable ingress
kubectl create namespace test
#kubectl create -f . -n test
kubectl create -n test \
 -f tangodb-pv.yaml \
 -f tangodb-pvc.yaml \
 -f tangodb-statefulset.yaml \
 -f tangodb-service.yaml \
 -f databaseds-statefulset.yaml \
 -f databaseds-service.yaml \
 -f tangotest.yaml \
 -f tango-vnc.yaml \
 -f tango-vnc-svc.yaml \
 -f tango-vnc-ingress.yaml \
 -f applauncher-sa.yaml \
 -f applauncher-role.yaml \
 -f applauncher-rolebinding.yaml
```

To verify that all worked:
```
kubectl get all -n test
```
or to keep it refreshed constantly:
```
watch kubectl get ingress,service,statefulset,pod,pvc,pv,sa,role,rolebinding -n test
```

This should show pods and services

## Services

### mysql server with tango table

Accessible within the cluster under name `tangodb-service.test`

### tango database DS

Accessible within the test namespace in the cluster under name `databaseds-service:10000`

### VNC
One of them is a noVNC machine serving a desktop-in-browser: to use it, point your browser to `http://<MINIKUBE_IP>/vnc_lite.html` and use password `TestVNC` .

The `<MINIKUBE_IP>` can be obtained with `minikube ip` 

Within that desktop you can e.g. run the Tango Java GUIs

## Apps

### Taurus GUI example01

By starting the pod gui-example01 (`kubectl create -n test -f gui-example01.yaml`), the GUI will be shown in the novnc desktop


## Clean up

In order to clean up, you can delete the created pods and services in the test namespace and shutdown minikube:

```
kubectl delete namespace/test
kubectl delete persistentvolume/tangodb-pv
minikube stop
```

And you may optionally remove the whole kubernetes cluster:

```
minikube delete
```


